import time
import logging
import argparse
from concurrent import futures

import grpc
from test_pb2_grpc import add_GenerateAudioServicer_to_server

from servicer import DummyServicer

_ONE_DAY_IN_SECONDS = 60 * 60 * 24

if __name__ == '__main__':
    parser = argparse.ArgumentParser(
        description="Dummy server")
    parser.add_argument('-s', '--sampling-rate',
                        nargs='?',
                        dest='sampling_rate',
                        default=16000,
                        help='Sampling rate of audio to generate',
                        type=int)
    parser.add_argument('-l', '--log-level',
                        nargs='?',
                        dest='log_level',
                        help='logger level',
                        type=str,
                        default='INFO')
    parser.add_argument('-p', '--port',
                        nargs='?',
                        dest='port',
                        help='grpc port',
                        type=int,
                        default=54321)
    args = parser.parse_args()

    servicer = DummyServicer(args)
    server = grpc.server(futures.ThreadPoolExecutor(max_workers=1), )
    add_GenerateAudioServicer_to_server(servicer, server)
    server.add_insecure_port('[::]:{}'.format(args.port))
    server.start()

    logging.basicConfig(
        level=getattr(logging, args.log_level),
        format='[%(levelname)s|%(filename)s:%(lineno)s][%(asctime)s] >>> %(message)s'
    )
    logging.info('Audio generator starting at 0.0.0.0:%d', args.port)

    try:
        while True:
            # Sleep forever, since `start` doesn't block
            time.sleep(_ONE_DAY_IN_SECONDS)
    except KeyboardInterrupt:
        server.stop(0)
