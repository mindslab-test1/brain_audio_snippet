import numpy as np

from wav_maker import WavMaker
from test_pb2 import WavBinary, AudioConfig
from test_pb2_grpc import GenerateAudioServicer

chunk_size = 3145728

def wav2bytes(wavmaker, wavlist):
    wav_int = np.int16(wavlist / (np.max(np.abs(wavlist)) + 0.001) * 32767)
    wav_bytes = wavmaker.make_header(len(wav_int))
    wav_bytes += wavmaker.make_data(wav_int)
    return wav_bytes

class DummyServicer(GenerateAudioServicer):
    def __init__(self, args):
        self.sampling_rate = args.sampling_rate
        self.wavmaker = WavMaker(sample_rate=self.sampling_rate)
        self.audio_config = AudioConfig(
            sampling_rate=self.sampling_rate
        )

    def GetAudio(self, request, context):
        audio_length = request.length

        time = np.arange(audio_length) / self.sampling_rate
        dummy_wav = np.clip(np.sin(2000*np.pi*time)*time, -1.0, 1.0)
        wav_bytes = wav2bytes(self.wavmaker, dummy_wav)

        for i in range((len(wav_bytes) - 1) // chunk_size + 1):
            yield WavBinary(bin=wav_bytes[chunk_size * i : chunk_size * (i + 1)])

    def GetAudioConfig(self, empty, context):
        return self.audio_config
