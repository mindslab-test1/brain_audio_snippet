# wav_maker

Some scripts to test `wav_maker.py`.

## Build proto

It is pre-built and tracked by git, as shown in `test_pb2.py` and `test_pb2_grpc.py`,
so you won't have to do this unless you're changing `test.proto`.

```bash
python -m grpc.tools.protoc --proto_path=. --python_out=. --grpc_python_out=. test.proto
```

## Execute server

```bash
python server.py -s <sampling-rate>
```

## Execute client for testing

```bash
python client.py
```

By opening `dummy_output.wav` with Audacity,
you're expected to see a sinusoidal wave with constant frequency 2000Hz and growing amplitude, which stops growing at 1.0.

