import io
import grpc
import argparse
import unittest
import numpy as np
from scipy.io.wavfile import read
import google.protobuf.empty_pb2 as empty

from test_pb2 import AudioRequest, WavBinary
from test_pb2_grpc import GenerateAudioStub


class DummyClient(object):
    def __init__(self, remote='127.0.0.1:54321', chunk_size=3145728):
        channel = grpc.insecure_channel(remote)
        self.stub = GenerateAudioStub(channel)
        self.chunk_size = chunk_size

    def get_audio(self, length):
        request = AudioRequest(length=length)
        return self.stub.GetAudio(request)

    def get_audio_config(self):
        return self.stub.GetAudioConfig(empty.Empty())


class TestClient(unittest.TestCase):
    def setUp(self):
        '''TODO
            I can't figure out a right way to pass CLI argument to here, so I'm assuming that
            (1) the server and the client will be running in same machine,
            (2) and the port is always 54321,
            so that using default remote='127.0.0.1:54321' will be okay
        '''
        self.client = DummyClient()

    def get_sampling_rate(self):
        response = self.client.get_audio_config()
        sampling_rate = response.sampling_rate
        return sampling_rate

    def read_response(self, response):
        wav_result = bytearray()
        for wav_binary in response:
            wav_result.extend(wav_binary.bin)
        wav_result = io.BytesIO(wav_result)
        sampling_rate, wav_result = read(wav_result)
        return sampling_rate, wav_result

    def test_output_shape(self):
        response = self.client.get_audio(12345)
        sampling_rate, wav_result = self.read_response(response)
        self.assertEqual(self.get_sampling_rate(), sampling_rate)
        self.assertEqual(12345, len(wav_result))

    def test_write_file(self):
        response = self.client.get_audio(23456)
        with open('dummy_output.wav', 'wb') as wf:
            for wav_binary in response:
                wf.write(wav_binary.bin)


if __name__ == '__main__':
    unittest.main()
