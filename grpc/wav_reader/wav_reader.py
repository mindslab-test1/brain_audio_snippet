import numpy as np
from scipy.io.wavfile import read


class WavBinaryWrapper(object):
    def __init__(self, data):
        super(WavBinaryWrapper, self).__init__()
        self.data = data
        self.position = 0

    def read(self, n=-1):
        if n == -1:
            data = self.data[self.position:]
            self.position = len(self.data)
            return data
        else:
            data = self.data[self.position:self.position+n]
            self.position += n
            return data

    def seek(self, offset, whence=0):
        if whence == 0:
            self.position = offset
        elif whence == 1:
            self.position += offset
        elif whence == 2:
            self.position = len(self.data) + offset
        else:
            raise RuntimeError('Invalid access')
        return self.position

    def tell(self):
        return self.position

    def close(self):
        pass

def wav_formatter(wav):
    if len(wav.shape) == 2:
        wav = wav[:, 0]

    if wav.dtype == np.int16:
        wav = wav / 32768.0
    elif wav.dtype == np.int32:
        wav = wav / 2147483648.0
    elif wav.dtype == np.uint8:
        wav = (wav - 128) / 128.0

    wav = wav.astype(np.float32)
    return wav

def wav_reader(wav_binary_iterator):
    wav = bytearray()
    for wav_binary in wav_binary_iterator:
        wav.extend(wav_binary.bin)

    wav = WavBinaryWrapper(wav)
    sampling_rate, wav = read(wav)
    wav = wav_formatter(wav)
    return sampling_rate, wav
