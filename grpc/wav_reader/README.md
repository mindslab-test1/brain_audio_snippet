# wav_reader

Some scripts to test `wav_reader` method in `wav_reader.py`.
Normally, the `wav_reader` method will be used at the server side.

`wav_reader` method within `wav_reader.py` uses the following process to read a wav binary stream:
- Step 1: Initialize `bytearray` to get wav binary from input iterator stream.
- Step 2: Wrap the `bytearray` with `WavBinaryWrapper`
- Step 3: Read the wrapped binary with `scipy.io.wavfile.read` method. We get `sr` and `wav` here.
- Step 4: Convert the audio into mono, `np.float32` type via `wav_formatter` method.

For now, no test script is written here. 
