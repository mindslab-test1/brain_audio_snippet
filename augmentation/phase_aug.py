import torch
import torch.nn as nn
import torch.nn.functional as F
import unittest


class PhaseAug(nn.Module):
    def __init__(self, nfft=1024, hop=256, std=1.):
        super().__init__()
        self.nfft = nfft
        self.hop = hop
        self.register_buffer('window', torch.hann_window(nfft))
        self.std = std

    #x: audio [B,T]
    @torch.no_grad()
    def forward(self, x):
        T = x.shape[1]
        x = F.pad(x, (0, self.nfft), 'constant', 0)
        stft = torch.stft(x,
                          self.nfft,
                          self.hop,
                          window=self.window,
                          )  #[B,F,T,2]
        mag = torch.norm(stft, p=2, dim=-1)  #[B,F,T]
        phase = torch.atan2(stft[..., 1], stft[..., 0])  #[B,F,T]
        phase += self.std * torch.randn([phase.shape[0], phase.shape[1]],
                                        device=x.device).unsqueeze(-1)
        x = torch.istft(mag.unsqueeze(-1) *
                        torch.stack([phase.cos(), phase.sin()], dim=-1),
                        self.nfft,
                        self.hop,
                        window=self.window,
                        )
        x = x[:, :T].detach()
        return x


class TestPhaseAug(unittest.TestCase):
    def test_io_shape(self, device='cpu'):
        phase_aug = PhaseAug().to(device)

        # random sized input
        dummy_input = torch.randn(3, 12345).to(device)
        output = phase_aug(dummy_input)
        self.assertSequenceEqual(output.shape, dummy_input.shape)

        # very short audio
        dummy_input = torch.randn(3, 1).to(device)
        output = phase_aug(dummy_input)
        self.assertSequenceEqual(output.shape, dummy_input.shape)

        # input length is multiple of hop size
        dummy_input = torch.randn(3, 256 * 123).to(device)
        output = phase_aug(dummy_input)
        self.assertSequenceEqual(output.shape, dummy_input.shape)

    def test_io_shape_cuda(self):
        self.test_io_shape(device='cuda')


if __name__ == '__main__':
    unittest.main()
