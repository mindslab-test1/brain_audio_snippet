import os
import glob
import tqdm
import hashlib
import argparse
from scipy.io.wavfile import read

# from https://stackoverflow.com/a/3431838/14931669
def md5(fname):
    hash_md5 = hashlib.md5()
    with open(fname, "rb") as f:
        for chunk in iter(lambda: f.read(4096), b""):
            hash_md5.update(chunk)
    return hash_md5.hexdigest() 

def read_metadata(metadata_path):
    print('='*50)
    print('Reading metadata from %s' % metadata_path)
    print('MD5 checksum of metadata: %s' % md5(metadata_path))
    with open(metadata_path, 'r', encoding='utf-8') as f:
        meta = [line.strip().split('|') for line in f.readlines()]
    return meta

def main(args):
    meta = read_metadata(args.metadata_path)
    total_len = 0.0
    speakers_len = {}
    min_audio_len = (1e9, '')
    max_audio_len = (0.0, '')

    print('Checking audio files and their sampling rate')
    for wavpath, _, speaker_id in tqdm.tqdm(meta):
        path = os.path.join(args.root_path, wavpath)
        sr, wav = read(path)
        assert sr == args.sampling_rate, \
            "Sampling rate mismatch at: %s " % path + \
            "Expected %d, got %d" % (args.sampling_rate, sr)

        if speaker_id not in speakers_len.keys():
            speakers_len[speaker_id] = 0.0

        audio_len = len(wav) / sr
        total_len += audio_len
        speakers_len[speaker_id] += audio_len
        if audio_len < min_audio_len[0]:
            min_audio_len = (audio_len, wavpath)
        if audio_len > max_audio_len[0]:
            max_audio_len = (audio_len, wavpath)

    print('Total number of audios: %d' % len(meta))
    print('Total number of people: %d' % len(speakers_len))
    print('Total length: %.1f hours' % (total_len / 3600))
    print('Shortest audio: %.2fs, %s' % min_audio_len)
    print('Longest audio: %.2fs, %s' % max_audio_len)
    min_speaker_id = min(speakers_len, key=speakers_len.get)
    max_speaker_id = max(speakers_len, key=speakers_len.get)
    print('Speaker with smallest data: %s, %.3f hours' % \
        (min_speaker_id, speakers_len[min_speaker_id] / 3600))
    print('Speaker with largest data: %s, %.3f hours' % \
        (max_speaker_id, speakers_len[max_speaker_id] / 3600))
    print('='*50)


if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument('-s', '--sampling_rate', type=int, required=True,
                        help="Sampling rate")
    parser.add_argument('-r', '--root_path', type=str, required=True,
                        help="root path of TTS data")
    parser.add_argument('-m', '--metadata_path', type=str, required=True,
                        help="absolute path of metadata")
    args = parser.parse_args()
    main(args)
